# repohomeothergoogle

this is a linking document for repositories that are stored in my 
bitbucket account. 

This document links to google stuff that is not connected to android development (which has a separate repo home)

**here are the repos**

I have written some code related to technologies of google that I have used but not related to android (which has its own separate home). I use this for working on client related projects and also for training. 

---

1. [https://bitbucket.org/thechalakas/googlesigninserverside]

---

## external links

visit my website here - [the chalakas](http://thechalakas.com)

visit my blog here - [the sanguine tech trainer](https://thesanguinetechtrainer.com)

find me on twiter here - [Jay (twitter)](https://twitter.com)

find me on instagram here - [Jay (instagram)](https://www.instagram.com/jay_instogrm/) 